﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;

    private float lastDogSpawnTime = -1;

    // Update is called once per frame
    void Update()
    {
        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space) && CanSpawnAgain())
        {
            Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
            lastDogSpawnTime = Time.time;
        }
    }

    bool CanSpawnAgain() => lastDogSpawnTime < 0 || Time.time - lastDogSpawnTime > 3.0f;
}
