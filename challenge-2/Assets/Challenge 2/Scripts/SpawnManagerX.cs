﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingIterator<T> : IEnumerator<T>
{
    public T Current => originalIterator.Current;

    protected IEnumerator<T> originalIterator;

    object IEnumerator.Current => throw new System.NotImplementedException();

    public LoopingIterator(IEnumerable<T> enumerable) {
        this.originalIterator = enumerable.GetEnumerator();
    }

    public void Dispose()
    {
        originalIterator.Dispose();
    }

    public bool MoveNext()
    {
        if (!originalIterator.MoveNext()) {
            Reset();
            // Populate the first item of the new iterator (recursive call, the if statement should be avoided this time)
            MoveNext();
        }
        return true;
    }

    public void Reset()
    {
        originalIterator.Reset();
    }
}

public class SpawnManagerX : MonoBehaviour
{
    public GameObject[] ballPrefabs;
    protected LoopingIterator<GameObject> ballIterator;

    private float spawnLimitXLeft = -22;
    private float spawnLimitXRight = 7;
    private float spawnPosY = 30;

    private float startDelay = 1.0f;
    private float spawnInterval = 4.0f;

    // Start is called before the first frame update
    void Start()
    {
        ballIterator = new LoopingIterator<GameObject>(ballPrefabs);
        Invoke("SpawnRandomBall", startDelay);
    }

    // Spawn random ball at random x position at top of play area
    void SpawnRandomBall ()
    {
        // Generate random ball index and random spawn position
        Vector3 spawnPos = new Vector3(Random.Range(spawnLimitXLeft, spawnLimitXRight), spawnPosY, 0);

        ballIterator.MoveNext();
        // instantiate ball at random spawn location
        Instantiate(ballIterator.Current, spawnPos, ballIterator.Current.transform.rotation);

        Invoke("SpawnRandomBall", Random.Range(3.0f, 5.0f));
    }

}
