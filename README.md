# Unity Learning

A repository of my first few Unity projects.

These projects were made while loosely following [these tutorials](https://learn.unity.com/course/create-with-code).