using UnityEngine;

public class PropellerSpinner : MonoBehaviour
{
    protected readonly Vector3 propellerRotation = new Vector3(0, 0, 1000);

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(propellerRotation * Time.deltaTime);
    }
}
