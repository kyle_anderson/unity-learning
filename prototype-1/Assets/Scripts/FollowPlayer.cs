using UnityEngine;
using System;

public class FollowPlayer : MonoBehaviour
{
    public GameObject player;

    private Vector3 cameraOffset = new Vector3(0, 7, -20);

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = player.transform.position + cameraOffset;
    }
}
