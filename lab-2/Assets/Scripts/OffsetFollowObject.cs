using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Follows the player with an offset. The offset is determined based on the initial
/// positional difference between this object and the object being followed.
/// </summary>
public class OffsetFollowObject : MonoBehaviour
{
    public GameObject objectToFollow;
    protected Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - objectToFollow.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = offset + objectToFollow.transform.position;
    }
}
