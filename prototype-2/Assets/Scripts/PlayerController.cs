using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float horizontalInput;
    private readonly Vector3 playerVelocity = Vector3.right * 15;
    private readonly float xBounds = 23f;
    public GameObject foodProjectilePrefab;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        bool isGoingLeft = horizontalInput < 0;
        bool isGoingRight = horizontalInput > 0;
        float currentX = transform.position.x;
        if ((-xBounds < currentX || isGoingRight) && (isGoingLeft || currentX < xBounds))
        {
            transform.Translate(playerVelocity * horizontalInput * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            Instantiate(foodProjectilePrefab, transform.position, foodProjectilePrefab.transform.rotation);
        }
    }
}
