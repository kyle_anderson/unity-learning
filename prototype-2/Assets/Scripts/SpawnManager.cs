using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopingIterator<T> : IEnumerator<T>
{
    public T Current => originalIterator.Current;

    protected IEnumerator<T> originalIterator;

    object IEnumerator.Current => throw new System.NotImplementedException();

    public LoopingIterator(IEnumerable<T> enumerable) {
        this.originalIterator = enumerable.GetEnumerator();
    }

    public void Dispose()
    {
        originalIterator.Dispose();
    }

    public bool MoveNext()
    {
        if (!originalIterator.MoveNext()) {
            Reset();
            // Populate the first item of the new iterator (recursive call, the if statement should be avoided this time)
            MoveNext();
        }
        return true;
    }

    public void Reset()
    {
        originalIterator.Reset();
    }
}

public class SpawnManager : MonoBehaviour
{
    public GameObject[] availableAnimals;
    protected LoopingIterator<GameObject> animalStream;
    public Tuple<float, float> spawnRangeX = new Tuple<float, float>(-20, 20);
    // Initially spawn the first animal after 1.5 seconds.
    private readonly float startDelay = 2f;
    // Then spawn a new animal every 2 seconds.
    private readonly float repeatDelay = 2f;

    public SpawnManager() {
    }

    // Start is called before the first frame update
    void Start()
    {
        animalStream = new LoopingIterator<GameObject>(availableAnimals);
        InvokeRepeating("SpawnAnimal", startDelay, repeatDelay);
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void SpawnAnimal()
    {
        animalStream.MoveNext();
        Instantiate(animalStream.Current, new Vector3(UnityEngine.Random.Range(spawnRangeX.Item1, spawnRangeX.Item2), 0, 20), animalStream.Current.transform.rotation);
    }
}
